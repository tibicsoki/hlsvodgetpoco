#include "stdafx.h"
#include "DownloadManager.h"

#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/TeeStream.h"

namespace m3ud
{

DownloadManager::DownloadManager(const Context::Ptr pocoContext_)
	: pocoContext(pocoContext_)
{
}


DownloadManager::~DownloadManager()
{
}

void DownloadManager::AddJob(const URI& uri, DownloadFunction job)
{
	auto key = make_tuple(uri.getScheme(), uri.getHost(), uri.getPort());

	if (contextMap.find(key) == contextMap.end())
	{
		if (uri.getScheme() == "http")
		{
			DownloadContext downloadContext(make_unique<HTTPClientSession>(uri.getHost(), uri.getPort()));
			contextMap.insert(std::make_pair(key, move(downloadContext)));
		}
		else if (uri.getScheme() == "https")
		{
			DownloadContext downloadContext(unique_ptr<HTTPClientSession>(make_unique<HTTPSClientSession>(uri.getHost(), uri.getPort(), pocoContext)));
			contextMap.insert(std::make_pair(key, move(downloadContext)));
		}
		else
			throw exception("Unknown scheme");
	}

	contextMap[key].GetJobQueue().AddJob(uri, job);
}

void DownloadManager::SaveToFile(const path& targetpath, const string& linkpath, istream& rs) const
{
	path plinkpath(linkpath);
	path outputfilepath = targetpath / plinkpath.filename().string();

	if (!exists(outputfilepath.parent_path()))
		create_directories(outputfilepath.parent_path());

	std::cout << "Saving stream to " << outputfilepath << std::endl;

	ofstream ofs(outputfilepath.string(), ofstream::out| ofstream::binary);
	ofs << rs.rdbuf();
	ofs.flush();
}

template<typename T>
void DownloadManager::ExtractAndSave(const path& targetpath, const string& linkpath, istream& rs, T& extractTo) const
{
	path plinkpath(linkpath);
	path outputfilepath = targetpath / plinkpath.filename().string();

	if (!exists(outputfilepath.parent_path()))
		create_directories(outputfilepath.parent_path());

	std::cout << "Saving stream to " << outputfilepath << std::endl;

	ofstream ofs(outputfilepath.string(), std::ofstream::out);

	Poco::TeeInputStream teeInput(rs);
	teeInput.addStream(ofs);
	teeInput >> extractTo;
}

void DownloadManager::ScheduleDownload(const URI& mainPlayListUri, const path& targetPath)
{
	AddJob(mainPlayListUri, [this, targetpath = targetPath](HTTPClientSession& session, const URI& uri) {
		// prepare path
		string linkpath(uri.getPathAndQuery());
		if (linkpath.empty()) linkpath = "/";

		// send request
		HTTPRequest req(HTTPRequest::HTTP_GET, linkpath, HTTPMessage::HTTP_1_1);
		session.sendRequest(req);

		// get response
		HTTPResponse res;
		istream& rs = session.receiveResponse(res);

		M3UMainPlayList pl(uri);
		
		ExtractAndSave(targetpath, linkpath, rs, pl);

		ScheduleDownload(pl, targetpath);
	});
}

void DownloadManager::ScheduleDownload(const M3UMainPlayList& mainPlayList, const path& targetPath)
{
	if (!exists(targetPath))
		create_directory(targetPath);

	for (const auto& stream : mainPlayList.GetEntries())
	{
		path targetpath = targetPath;
		if (stream.uri.isRelative())
			targetpath /= path(stream.uri.toString()).parent_path();

		URI streamAbsUri(stream.uri.isRelative()? URI(mainPlayList.GetURI(), stream.uri.toString()):stream.uri);

		AddJob(streamAbsUri, [this, targetpath](HTTPClientSession& session, const URI& uri) {
			// prepare path
			string linkpath(uri.getPathAndQuery());
			if (linkpath.empty()) linkpath = "/";

			// send request
			HTTPRequest req(HTTPRequest::HTTP_GET, linkpath, HTTPMessage::HTTP_1_1);
			session.sendRequest(req);

			// get response
			HTTPResponse res;
			istream& rs = session.receiveResponse(res);
			M3UPlayList pl(uri);

			ExtractAndSave(targetpath, linkpath, rs, pl);

			ScheduleDownload(pl, targetpath);
		});
	}
}

void DownloadManager::ScheduleDownload(const M3UPlayList& playList, const path& targetPath)
{
	if (!exists(targetPath))
		create_directory(targetPath);

	for (const auto& stream : playList.GetEntries())
	{
		path targetpath = targetPath;
		if (stream.uri.isRelative())
			targetpath /= path(stream.uri.toString()).parent_path();
		URI streamAbsUri(stream.uri.isRelative() ? URI(playList.GetURI(), stream.uri.toString()) : stream.uri);
		AddJob(streamAbsUri, [this, targetpath](HTTPClientSession& session, const URI& uri) {
			// prepare path
			string linkpath(uri.getPathAndQuery());
			if (linkpath.empty()) linkpath = "/";

			// send request
			HTTPRequest req(HTTPRequest::HTTP_GET, linkpath, HTTPMessage::HTTP_1_1);
			session.sendRequest(req);

			// get response
			HTTPResponse res;
			istream& rs = session.receiveResponse(res);

			SaveToFile(targetpath, linkpath, rs);
		});
	}
}

size_t DownloadManager::JobsToRun() const
{
	return std::accumulate(contextMap.cbegin(), contextMap.cend(), 0, [](size_t val, const auto& p) {return val + p.second.JobsToRun(); });
}

void DownloadManager::RunOneJob()
{
	for (auto& p : contextMap)
	{
		auto& jobQueue = p.second.GetJobQueue();
		if (jobQueue.JobsToRun() > 0)
			jobQueue.ExecuteFirstJob(p.second.GetSession());
	}
}


}