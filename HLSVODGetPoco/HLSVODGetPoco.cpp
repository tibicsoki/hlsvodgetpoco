// HLSVODGetPoco.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <iostream>

#include <DownloadManager.h>
#include <Poco/Exception.h>

using namespace std;
using namespace m3ud;
using namespace Poco;


string getArgument(const string& argument, const string& regexstr)
{
	regex argregex(regexstr);
	smatch match;
	if (!std::regex_match(argument, match, argregex))
		throw exception("Invalid argument");

	return match[1];
}

int main(int argc, char *argv[])
{
	if (argc != 3)
	{
		std::cout << "Usage: hlsvodgetpoco -i=[link to m3u8 main playlist -o=[output folder name]" << endl;
		exit(1);
	}

	/*
	https://bitdash-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.m3u8
	https://devstreaming-cdn.apple.com/videos/streaming/examples/bipbop_4x3/bipbop_4x3_variant.m3u8
	https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8
	https://mnmedias.api.telequebec.tv/m3u8/29880.m3u8
	http://184.72.239.149/vod/smil:BigBuckBunny.smil/playlist.m3u8
	http://www.streambox.fr/playlists/test_001/stream.m3u8
	*/

	try {

		string linkArgument = getArgument(argv[1], "\\s*\\-i\\s*=\\s*(.*)");
		string targetFolderArgument = getArgument(argv[2], "\\s*\\-o\\s*=\\s*(.*)");

		const Context::Ptr context = new Context(Context::CLIENT_USE, "", "", "", Context::VERIFY_NONE, 9, false, "ALL:!ADH:!LOW:!EXP:!MD5:@STRENGTH");
		URI uri(linkArgument);

		m3ud::DownloadManager downloadManager(context);

		downloadManager.ScheduleDownload(uri, targetFolderArgument);

		while (downloadManager.JobsToRun() > 0)
		{
			downloadManager.RunOneJob();
		}
	}
	catch (const Exception &e)
	{
		cerr << e.displayText() << endl;
	}
	catch (const exception &e)
	{
		cerr << e.what() << endl;
	}
}


