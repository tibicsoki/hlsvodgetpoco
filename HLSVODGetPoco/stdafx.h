// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>

#include <vector>
#include <string>
#include <istream>
#include <map>
#include <cassert>
#include <deque>
#include <functional>
#include <regex>
#include <memory>
#include <tuple>
#include <numeric>
#include <experimental/filesystem>
#include <fstream>

using std::vector;
using std::deque;
using std::function;
using std::map;
using std::unique_ptr;

using std::string;
using std::regex;
using std::smatch;
using std::istream;
using std::exception;
using std::sregex_token_iterator;
using std::tuple;
using std::ofstream;

using std::make_unique;
using std::move;
using std::experimental::filesystem::path;
using std::experimental::filesystem::create_directory;
using std::experimental::filesystem::create_directories;
using std::experimental::filesystem::exists;

