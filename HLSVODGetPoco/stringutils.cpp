#include "stdafx.h"
#include "stringutils.h"

namespace m3ud
{

vector<string> split(const string& input, const string& regexstr)
{
	// passing -1 as the submatch index parameter performs splitting
	regex re(regexstr);
	std::sregex_token_iterator
		first{ input.begin(), input.end(), re, -1 },
		last;
	return { first, last };
}

}
