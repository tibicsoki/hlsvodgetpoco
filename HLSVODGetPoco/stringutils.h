#pragma once

namespace m3ud
{
	vector<string> split(const string& input, const string& regex);

	inline void SkipEmptyLines(istream& is, string& str)
	{
		for (str = ""; (str.find_first_not_of("\r\t\n ") == string::npos) && is; getline(is, str));
	}

	inline string GetParentPath(const string& uriStr)
	{
		size_t i = uriStr.rfind("/", uriStr.length());
		return (i != string::npos) ? uriStr.substr(0, i) : "";
	}
}

