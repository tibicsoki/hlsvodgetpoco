#pragma once

#include "M3UPlayList.h"
#include "M3UMainPlayList.h"
#include "DownloadContext.h"

#include "Poco/Net/SSLManager.h"
#include "Poco/Net/HTTPSClientSession.h"

namespace m3ud
{

using Poco::Net::Context;
using Poco::Net::HTTPClientSession;

class DownloadManager
{
public:
	DownloadManager(const Context::Ptr);
	~DownloadManager();

	void ScheduleDownload(const URI&             mainPlayListUri, const path& targetPath);
	void ScheduleDownload(const M3UMainPlayList& mainPlayList,    const path& targetPath);
	void ScheduleDownload(const M3UPlayList&     playList,        const path& targetPath);

	template<typename T>
	void ExtractAndSave(const path& targetpath, const string& linkpath, istream& rs, T& extractTo) const;

	void SaveToFile(const path& targetpath, const string& linkpath, istream& rs) const;

	size_t JobsToRun() const;
	void RunOneJob();

private:
	const Context::Ptr pocoContext;

	void AddJob(const URI& uri, DownloadFunction job);
	using ContextMapKey = tuple<string, string, unsigned short>;
	map< ContextMapKey, DownloadContext > contextMap;
};

}

