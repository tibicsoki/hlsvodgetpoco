#pragma once

#include "DownloadJobQueue.h"
#include "Poco/Net/HTTPClientSession.h"

namespace m3ud
{

using namespace Poco::Net;

class DownloadContext
{
public:
	DownloadContext(unique_ptr<HTTPClientSession> session_);
	DownloadContext();
	DownloadContext(DownloadContext&& other);
	~DownloadContext() = default;

	HTTPClientSession& GetSession() const;
	DownloadJobQueue& GetJobQueue();

	size_t JobsToRun() const;

private:
	DownloadJobQueue              jobQueue;
	unique_ptr<HTTPClientSession> session;
};

}

