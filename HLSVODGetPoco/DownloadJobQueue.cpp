#include "stdafx.h"
#include "DownloadJobQueue.h"

namespace m3ud
{

DownloadJobQueue::DownloadJobQueue()
{
}


DownloadJobQueue::~DownloadJobQueue()
{
}

void DownloadJobQueue::AddJob(const URI& uri, DownloadFunction job)
{
	jobs.emplace_back( uri, job );
}

void DownloadJobQueue::ExecuteFirstJob(HTTPClientSession& session)
{
	if ( jobs.size() > 0 )
	{
		DownloadJob first = jobs.front();
		jobs.pop_front();
		first.job(session, first.uri);
	}
}

bool DownloadJobQueue::IsJobLeft() const
{
	return jobs.size() > 0;
}

}