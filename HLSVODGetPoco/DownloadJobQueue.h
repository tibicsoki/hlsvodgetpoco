#pragma once

#include <Poco/URI.h>
#include "Poco/Net/HTTPClientSession.h"

namespace m3ud
{

class DownloadManager;
using Poco::URI;
using Poco::Net::HTTPClientSession;
using DownloadFunction = function< void(HTTPClientSession&, const URI&) >;

struct DownloadJob
{
	URI              uri;
	DownloadFunction job;

	DownloadJob(const URI& uri_, DownloadFunction job_)
		: uri(uri_), job(job_)
	{
	}
};

class DownloadJobQueue
{
public:
	DownloadJobQueue();
	DownloadJobQueue(DownloadJobQueue&& other)
		: jobs(move(other.jobs))
	{
	}
	~DownloadJobQueue();

	void AddJob( const URI& uri, DownloadFunction job );

	void ExecuteFirstJob(HTTPClientSession& session);

	bool IsJobLeft() const;

	size_t JobsToRun() const { return jobs.size(); }

private:
	deque< DownloadJob > jobs;
};

}

