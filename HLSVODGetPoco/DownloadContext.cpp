#include "stdafx.h"
#include "DownloadContext.h"

namespace m3ud
{

DownloadContext::DownloadContext(unique_ptr<HTTPClientSession> session_)
	: session(std::move(session_))
{
}
DownloadContext::DownloadContext() : session(nullptr)
{
}
DownloadContext::DownloadContext(DownloadContext&& other)
	: jobQueue(move(other.jobQueue))
	, session(move(other.session))
{

}

HTTPClientSession& DownloadContext::GetSession() const
{
	return *session.get();
}

DownloadJobQueue& DownloadContext::GetJobQueue()
{
	return jobQueue;
}

size_t DownloadContext::JobsToRun() const 
{ 
	return jobQueue.JobsToRun(); 
}

}