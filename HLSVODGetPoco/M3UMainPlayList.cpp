#include "stdafx.h"
#include "M3UMainPlayList.h"
#include "RegexConstants.h"

namespace m3ud
{

istream& operator>>(istream& is, M3UMainPlayList& dt)
{
	string str;

	SkipEmptyLines(is, str);

	if (!dt.header.CanParseEXTM3ULiteral(str))
		throw exception("Syntax error: cannot find EXTM3U literal");

	SkipEmptyLines(is, str);

	M3UMainEntry mainEntry;
	M3UMainExtXMedia xMedia;

	while (is)
	{
		if (mainEntry.CanParseFrom(str))
		{
			SkipEmptyLines(is, str);
			mainEntry.uri = str;
			dt.entries.emplace_back(move(mainEntry));
		}
		else if (xMedia.CanParse(str))
			dt.media.emplace_back(move(xMedia));
		else if (dt.header.CanParseProperties(str));
		else
			throw exception("Unknown entry in M3U main playlist");

		SkipEmptyLines(is, str);
	}

	return is;
}

//-----------------------------------------------------------------------------------------------

bool M3UMainHeader::CanParseEXTM3ULiteral(const string& str)
{
	const static regex EXTM3U_regex(EXTM3U_regex_str);
	smatch match;

	assert(values.empty());

	return regex_match(str, match, EXTM3U_regex);
}

bool M3UMainHeader::CanParseProperties(const string& str)
{
	regex header_property_regex(header_property_regex_str);
	smatch match;

	if (!regex_match(str, match, header_property_regex))
		return false;

	assert(values.empty());

	values[match[1]] = match[2];

	return true;
}

//-----------------------------------------------------------------------------------------------

bool M3UMainExtXMedia::CanParse(const string& str)
{
	const static regex EXT_MEDIA_regex(EXT_MEDIA_regex_str);
	smatch match;
	if (!regex_match(str, match, EXT_MEDIA_regex))
		return false;

	assert(values.empty());

	string keyvalues = match[1];

	const static regex EXT_MEDIA_props_regex(EXT_props_regex_str);
	sregex_token_iterator ritk(keyvalues.begin(), keyvalues.end(), EXT_MEDIA_props_regex, 1);
	sregex_token_iterator ritv(keyvalues.begin(), keyvalues.end(), EXT_MEDIA_props_regex, 2);
	sregex_token_iterator rend;

	while (ritk != rend && ritv != rend)
	{
		values[ritk->str()] = ritv->str();
		++ritk, ++ritv;
	}

	return true;
}

//-----------------------------------------------------------------------------------------------

M3UMainEntry::M3UMainEntry(M3UMainEntry&& other)
	: values(move(other.values))
	, uri(move(other.uri))
{
	other.uri.clear();
	assert(other.values.empty());
}

bool M3UMainEntry::CanParseFrom(const string& str)
{
	const static regex main_entry_regex(main_entry_regex_str);
	smatch match;
	if (!regex_match(str, match, main_entry_regex))
		return false;

	assert(values.empty());
	assert(uri.empty());

	string keyvalues = match[1];

	regex properties_regex(EXT_props_regex_str);
	sregex_token_iterator ritk(keyvalues.begin(), keyvalues.end(), properties_regex, 1);
	sregex_token_iterator ritv(keyvalues.begin(), keyvalues.end(), properties_regex, 2);
	sregex_token_iterator rend;

	while (ritk != rend && ritv != rend)
	{
		values[ritk->str()] = ritv->str();
		++ritk, ++ritv;
	}

	return true;
}

}
