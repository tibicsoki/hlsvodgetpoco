#pragma once


namespace m3ud
{
	constexpr const char* EXTM3U_regex_str          = "\\s*#\\s*EXTM3U\\s*";
	constexpr const char* header_property_regex_str = "\\s*#\\s*([a-zA-Z0-9\\-^:]*)\\s*\\:\\s*(.*)\\s*";
	constexpr const char* EXTINF_regex_str          = "\\s*#\\s*EXTINF\\s*:(.*)\\s*";
	constexpr const char* EXT_ENDLINE_regex_str     = "\\s*#\\s*EXT\\-X\\-ENDLIST\\s*";
	constexpr const char* EXT_MEDIA_regex_str       = "\\s*#\\s*EXT\\-X\\-MEDIA\\s*\\:(.*)";
	constexpr const char* EXT_props_regex_str       = "\\s*([a-zA-Z0-9\\-^=]*)\\s*\\=\\s*((\\\"[^\\\"]*\\\"[^,]*)|([^,]*))";
	constexpr const char* main_entry_regex_str      = "\\s*#\\s*EXT\\-X\\-STREAM\\-INF\\s*\\:(.*)";
}