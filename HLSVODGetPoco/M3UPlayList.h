
#pragma once

#include "stringutils.h"
#include <Poco/URI.h>

namespace m3ud
{

struct M3UHeader
{
	map<string, string> values;

	M3UHeader() = default;
	M3UHeader(const M3UHeader& other) = delete;
	M3UHeader(M3UHeader&& other) = default;

	bool CanParseEXTM3ULiteral(const string& str);
	bool CanParseProperties(const string& str);
};

struct M3UEntry
{
	vector<string> values;
	Poco::URI      uri;

	M3UEntry() = default;
	M3UEntry(const M3UEntry& other) = delete;
	M3UEntry(M3UEntry&& other);

	bool CanParseFrom(const string& str);
	bool IsEndLine(const string& str);
};

class M3UPlayList
{
private:
	M3UHeader         header;
	vector<M3UEntry>  entries;
	Poco::URI         uri;

public:
	M3UPlayList(const Poco::URI& uri_)
		: uri(uri_)
	{
	}
	M3UPlayList(const M3UPlayList& other) = delete;
	M3UPlayList(M3UPlayList&& other) = default;
	~M3UPlayList() {};

	const vector<M3UEntry>&      GetEntries()    const { return entries; }
	const Poco::URI&             GetURI()        const { return uri; }

	friend istream& operator>>(istream& is, M3UPlayList& dt);
};

istream& operator>>(istream& is, M3UPlayList& dt);


}
