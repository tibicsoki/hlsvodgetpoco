
#pragma once

#include "stringutils.h"
#include <Poco/URI.h>

namespace m3ud
{

struct M3UMainHeader
{
	map<string, string> values;

	M3UMainHeader() = default;
	M3UMainHeader(const M3UMainHeader& other) = delete;
	M3UMainHeader(M3UMainHeader&& other) = default;

	bool CanParseEXTM3ULiteral(const string& str);
	bool CanParseProperties(const string& str);
};

struct M3UMainExtXMedia
{
	map<string, string> values;

	M3UMainExtXMedia() = default;
	M3UMainExtXMedia(const M3UMainExtXMedia& other) = delete;
	M3UMainExtXMedia(M3UMainExtXMedia&& other) = default;

	bool CanParse(const string& str);
};

struct M3UMainEntry
{
	map<string, string> values;
	Poco::URI           uri;

	M3UMainEntry() = default;
	M3UMainEntry(const M3UMainEntry& other) = delete;
	M3UMainEntry(M3UMainEntry&& other);

	bool CanParseFrom(const string& str);
};

class M3UMainPlayList
{
private:
	M3UMainHeader             header;
	vector<M3UMainExtXMedia>  media;
	vector<M3UMainEntry>      entries;
	Poco::URI                 uri;

public:

	M3UMainPlayList(const Poco::URI& uri_)
	: uri(uri_)
	{
	}
	M3UMainPlayList(const M3UMainPlayList& other) = delete;
	M3UMainPlayList(M3UMainPlayList&& other) = default;
	~M3UMainPlayList() {};

	const M3UMainHeader&             GetHeader()     const { return header; }
	const vector<M3UMainExtXMedia>&  GetMediaInfo()  const { return media; }
	const vector<M3UMainEntry>&      GetEntries()    const { return entries; }
	const Poco::URI&                 GetURI()        const { return uri; }

	friend istream& operator>>(istream& is, M3UMainPlayList& dt);
};

istream& operator>>(istream& is, M3UMainPlayList& dt);

}
