#include "stdafx.h"
#include "M3UPlayList.h"
#include "RegexConstants.h"

namespace m3ud
{

bool M3UHeader::CanParseEXTM3ULiteral(const string& str)
{
	const static regex EXTM3U_regex(EXTM3U_regex_str);
	smatch match;

	assert(values.empty());

	return regex_match(str, match, EXTM3U_regex);
}

bool M3UHeader::CanParseProperties(const string& str)
{
	regex header_property_regex(header_property_regex_str);
	smatch match;

	if (!regex_match(str, match, header_property_regex))
		return false;

	values[match[1]] = match[2];

	return true;
}

//----------------------------------------------------------------------------------------------

M3UEntry::M3UEntry(M3UEntry&& other)
	: values(move(other.values))
	, uri(move(other.uri))
{
	other.uri.clear();
	assert(other.values.empty());
}

bool M3UEntry::CanParseFrom(const string& str)
{
	const static regex EXTINF_regex(EXTINF_regex_str);
	smatch match;
	if (!regex_match(str, match, EXTINF_regex))
		return false;

	assert(values.empty());
	assert(uri.empty());

	string keyvalues = match[1];

	auto splitvalues = split(keyvalues, ",\\s*");

	for (auto& value : splitvalues)
		values.emplace_back(value);

	return true;
}

bool M3UEntry::IsEndLine(const string& str)
{
	const static regex EXT_ENDLINE_regex(EXT_ENDLINE_regex_str);
	smatch match;
	return regex_match(str, match, EXT_ENDLINE_regex);
}

istream& operator>>(istream& is, M3UPlayList& dt)
{
	string str;

	SkipEmptyLines(is, str);

	if (!dt.header.CanParseEXTM3ULiteral(str))
		throw exception("Syntax error: cannot find EXTM3U literal");

	SkipEmptyLines(is, str);

	M3UEntry entry;

	while (is)
	{
		if (entry.CanParseFrom(str))
		{
			SkipEmptyLines(is, str);
			entry.uri = str;
			dt.entries.emplace_back(move(entry));
		}
		else if (dt.header.CanParseProperties(str));
		else if (entry.IsEndLine(str)) {
			break;
		}
		else 
			throw exception("Unknown entry in M3U playlist");

		SkipEmptyLines(is, str);
	}

	return is;
}

//----------------------------------------------------------------------------------------------




}
